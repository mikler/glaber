#/bin/bash

ls ~/.ssh/glaberkey.pub || ssh-keygen -t ed25519 -f ~/.ssh/glaberkey -q -N ""

echo "instance-id: {}; local-hostname: glaber" > meta-data

cat <<EOF> user-data
#cloud-config
password: paSSw0rd
ssh_pwauth: true
ssh_authorized_keys:
  - $(cat ~/.ssh/glaberkey.pub)
chpasswd:
  expire: false

locale: en_US.UTF-8
locale_configfile: /etc/default/locale
EOF

genisoimage -output seed.img -volid cidata -joliet -rock user-data meta-data
