/*
** Copyright Glaber
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**/

#ifndef GLB_ASYNC_FORK
#define GLB_ASYNC_FORK

#include <iostream>
#include <vector>
#include <boost/asio.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

class async_fork {

    public:
        template <typename FUNC, typename ARG>
        void register_periodic_callback(FUNC,ARG);

        void run();
    private:
        boost::asio::io_service io;
       
        //will hold all the planned timers objects here
        std::vector<boost::asio::deadline_timer> recurring_timers; 
};

template <typename FUNC, typename ARG>
void async_fork::register_periodic_callback(FUNC, ARG) {
    //need to create an instance of timer, init it and store in the vector
    //alsow need to store and pass thread-specific data 
   //recurring_timers.push_back()
   
}

void async_fork::run() {
    LOG_INF("Async fork runner is started");
}
#endif