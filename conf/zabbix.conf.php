<?php

global $DB;
global $UI;

$DB["TYPE"]                             = 'POSTGRESQL';
#$DB["TYPE"]                             = 'MYSQL';

$DB["SERVER"]                   = '127.0.0.1'; 
#$DB["PORT"]                     = '5432'; //uncomment and set if non-default

$DB["SCHEMA"]                   = 'zabbix'; //no required for MySQL
$DB["DATABASE"]                 = 'glaber_srv';

$DB["USER"]                     = 'glaber';   //DB Access credentials
$DB["PASSWORD"]                 = 'glaber';

$ZBX_SERVER                    = '127.0.0.1'; //ip credentials of the server
$ZBX_SERVER_PORT                = '10055';

//name for showing in UI, not necessary for functionality                                
$ZBX_SERVER_NAME                = $_SERVER['SERVER_NAME'] . '(' . gethostname() . ')';
$IMAGE_FORMAT_DEFAULT   = IMAGE_FORMAT_PNG;

//to forbid edit inherited objects (templated or discovered)
//$UI['forbid_inherited_edit'] = 1

//allow fetch and display problems list from events histrory table in problems* widgets
//not recomended, overloads DB too much, there is no practical reason to show problems in widgets from history
//$UI['allow_problems_from_hitstory_in_widgets'] = 1;

//notify that problems screen will not be autorefreshed when problems are selected from the events 
//$UI['notify_problems_no_refresh'] = 1;
//show disabled items in latest view panel
//$UI['show_disabled_items_in_latest_data'] = 1;

?>
