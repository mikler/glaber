<?php declare(strict_types = 0);
/*
** Glaber
** Copyright (C) 2001-2024 Glaber
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**/
namespace Widgets\ProblemHosts\Actions;

use APP, CControllerResponseData,
	CRoleHelper, CArrayHelper;

require_once APP::getRootDir().'/include/blocks.inc.php';

class WidgetHostsView extends \CController {
	protected $hostids;
	protected $severitites;

	protected function init() {
		$this->disableCSRFValidation();
	}

	protected function checkPermissions(): bool {
		return $this->getUserType() >= USER_TYPE_ZABBIX_USER;
	}

	protected function checkInput(): bool {
	
		$fields = [
			'groupid' =>		'db hstgrp.groupid',
			'hostids' => 'array',
			'severities' =>'array',
			'compact_counters' => 'in 0,1'
		];

		return	 $this->validateInput($fields);
	}

	protected function doAction(): void {

		$this->hostids =  $this->getInput('hostids');
		$this->severities =  $this->getInput('severities');
		$this->groupid = $this->getInput('groupid');
		$this->compact_counters = $this->getInput('compact_counters');

		$trigger_states = \CZabbixServer::getHostTriggerCounters(\CSessionHelper::getId(), $this->hostids);
		
		$groups=[$this->groupid];
		[$groups, $hosts] = expandGroupsHostsFromFilters($groups , $this->hostids);

		CArrayHelper::sort($hosts, [['field' => 'name', 'order' => ZBX_SORT_UP]]);
		
		foreach ($trigger_states as $idx => $state) 
			if (isset($hosts[$state['hostid']]))
				$hosts[$state['hostid']]['problems_by_severity'] = $state['problems_by_severity'];
		
		
		foreach ($hosts as $host) 
			if ($host['maintenance_status'] == HOST_MAINTENANCE_STATUS_ON) 
				$maintenanceids[$host['maintenanceid']] = true;
		
		$maintenanceids = [];

		$maintenances = $maintenanceids
			? API::Maintenance()->get([
				'output' => ['name', 'description'],
				'maintenanceids' => array_keys($maintenanceids),
				'preservekeys' => true
			])
			: [];
		
		$this->setResponse(new CControllerResponseData([
			'name' => "The list of the hosts",
			'hosts' => $hosts,
			'maintenances' => $maintenances,
			'compact_counters' => $this->compact_counters,
			'severities' => $this->severities,
			'allowed_ui_problems' => $this->checkAccess(CRoleHelper::UI_MONITORING_PROBLEMS),
		]));

	}
}
