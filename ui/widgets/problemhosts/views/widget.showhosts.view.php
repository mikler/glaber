<?php declare(strict_types = 0);
/*
** Glaber
** Copyright (C) 2001-2024 Glaber
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
**/

$url = $data['allowed_ui_problems']
	? (new CUrl('zabbix.php'))
		->setArgument('action', 'problem.view')
		->setArgument('filter_set', '1')
		->setArgument('show', TRIGGERS_OPTION_RECENT_PROBLEM)
	: null;

$header = [_('Host')];

if ($data['compact_counters'])
	$header [] =  _('Problems');
else
	foreach (range(TRIGGER_SEVERITY_COUNT - 1, TRIGGER_SEVERITY_NOT_CLASSIFIED) as $severity) {
		if (in_array($severity, $data['severities'])) {
			$header[] = CSeverityHelper::getName($severity);
		}
	}

$table_inf = (new CTableInfo())->setHeader($header);

	$popup_rows = 0;

	foreach ($data['hosts'] as $hostid => $host) {
		
		if ($url !== null) {
			$url->setArgument('hostids', [$hostid]);
			$host_name = new CLink($host['name'], $url->getUrl());
		}
		else {
			$host_name = $host['name'];
		}

		if ($host['maintenance_status'] == HOST_MAINTENANCE_STATUS_ON) {
			if (array_key_exists($host['maintenanceid'], $data['maintenances'])) {
				$maintenance = $data['maintenances'][$host['maintenanceid']];
				$maintenance_icon = makeMaintenanceIcon($host['maintenance_type'], $maintenance['name'],
					$maintenance['description']
				);
			}
			else {
				$maintenance_icon = makeMaintenanceIcon($host['maintenance_type'], _('Inaccessible maintenance'),
					''
				);
			}

			$host_name = [$host_name, $maintenance_icon];
		}

		$row = new CRow((new CCol($host_name))->addClass(ZBX_STYLE_NOWRAP));
		if ($data['compact_counters']) {
			$row->addItem(new CProblemsList($host['problems_by_severity']));
		} else {

			foreach (range(TRIGGER_SEVERITY_COUNT - 1, TRIGGER_SEVERITY_NOT_CLASSIFIED) as $severity) {
				if (in_array($severity, $data['severities'])) {
					$row->addItem(
							($host['problems_by_severity'][$severity] != 0)
							? (new CCol($host['problems_by_severity'][$severity]))
								->addClass(CSeverityHelper::getStyle($severity))
							: ''
					);
				}
			}
		}

		$table_inf->addRow($row);

		if (++$popup_rows == ZBX_WIDGET_ROWS) {
			break;
		}
	}

$output = [
	'header' => "Hosts problems",
	'body' => $table_inf->toString(),
    'buttons' => [
	]
];

echo json_encode($output);
