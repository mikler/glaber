<?php declare(strict_types = 0);
/*
** Glaber
** Copyright (C) 2001-2024 Glaber
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
**/

$url_details = $data['allowed']['ui_problems'] ?
 	$url_details =  (new CUrl('tr_events.php'))
 		->setArgument('triggerid', '')
 		->setArgument('eventid', ''): null;

$header_time = new CColHeader([_('Time'), (new CSpan())->addClass(ZBX_STYLE_ARROW_DOWN)]);

$show_timeline = (array_key_exists('show_timeline', $data['filters']) && $data['filters']['show_timeline']);
$show_opdata = (array_key_exists('show_opdata', $data['filters'])) ? $data['filters']['show_opdata'] : OPERATIONAL_DATA_SHOW_NONE;
$show_tags = (array_key_exists('show_tags', $data['filters'])) ? $data['filters']['show_tags'] : false;

if ($show_timeline) {
	$header = [
		$header_time->addClass(ZBX_STYLE_RIGHT),
	 	(new CColHeader())->addClass(ZBX_STYLE_TIMELINE_TH),
	 	(new CColHeader())->addClass(ZBX_STYLE_TIMELINE_TH)
	 ];
} else {
	$header = [$header_time];
}

$table = (new CTableInfo())
 	->setHeader(array_merge($header, [
 	_('Info'),
 	_('Host'),
 	_('Problem'),
 	($show_opdata == OPERATIONAL_DATA_SHOW_SEPARATELY) ? _('Operational data') : null,
 	_('Duration'),
 	_('Update'),
 	_('Actions'),
 	($show_tags ) ? _('Tags'): null
 ]));

$tdata = [
 	'last_clock' => 0,
 	'sortorder' => ZBX_SORT_DOWN,
 	'show_three_columns' => false,
 	'show_two_columns' => false
];

$today = strtotime('today');

if ($data['problems']) {
	$objectids = [];

	foreach ($data['problems'] as $problem) {
	 	$objectids[$problem['objectid']] = true;
 	}

	$triggers = array_intersect_key($data['triggers'], $objectids);
}

$triggers_hosts = getTriggersHostsList($triggers);
$triggers_hosts = makeTriggersHostsList($triggers_hosts);

if ($show_tags)
	$tags = makeTags($data['problems']);

if (array_key_exists('show_suppressed', $data['filters']) && $data['filters']['show_suppressed']) {
	CScreenProblem::addSuppressionNames($problems);
}

foreach ($data['problems'] as $problem) {
	$trigger = $data['triggers'][$problem['objectid']];

	$cell_clock = ($problem['clock'] >= $today)
	 	? zbx_date2str(TIME_FORMAT_SECONDS, $problem['clock'])
	 	: zbx_date2str(DATE_TIME_FORMAT_SECONDS, $problem['clock']);

	if ($url_details !== null) {
	 	$url_details
 		->setArgument('triggerid', $problem['objectid'])
 		->setArgument('eventid', $problem['eventid']);
	 	$cell_clock = new CCol(new CLink($cell_clock, $url_details));
 	} else {
	 	$cell_clock = new CCol($cell_clock);
	}

 	if ($show_timeline) {
 		if ($tdata['last_clock'] != 0) {
	 		CScreenProblem::addTimelineBreakpoint($table, $tdata, $problem, false, false);
 		}
 		$tdata['last_clock'] = $problem['clock'];

 		$row = [
 			$cell_clock->addClass(ZBX_STYLE_TIMELINE_DATE),
 			(new CCol())
 				->addClass(ZBX_STYLE_TIMELINE_AXIS)
 				->addClass(ZBX_STYLE_TIMELINE_DOT),
 			(new CCol())->addClass(ZBX_STYLE_TIMELINE_TD)
 		];
	} else {
 	$row = [
 		$cell_clock
 			->addClass(ZBX_STYLE_NOWRAP)
 			->addClass(ZBX_STYLE_RIGHT)
 	];
}

$info_icons = [];

if (array_key_exists('suppression_data', $problem)) {
	if (count($problem['suppression_data']) == 1
 			&& $problem['suppression_data'][0]['maintenanceid'] == 0
 			&& isEventRecentlyUnsuppressed($problem['acknowledges'], $unsuppression_action)) {

		// Show blinking button if the last manual suppression was recently revoked.
		$user_unsuppressed = array_key_exists($unsuppression_action['userid'], $actions['users'])
 			? getUserFullname($actions['users'][$unsuppression_action['userid']])
 			: _('Inaccessible user');

 		$info_icons[] = (new CSimpleButton())
 			->addClass(ZBX_STYLE_ACTION_ICON_UNSUPPRESS)
 			->addClass('blink')
 			->setHint(_s('Unsuppressed by: %1$s', $user_unsuppressed));
 	} 	elseif ($problem['suppression_data']) {
 		$info_icons[] = makeSuppressedProblemIcon($problem['suppression_data'], false);
 	}
 	elseif (isEventRecentlySuppressed($problem['acknowledges'], $suppression_action)) {
 		// Show blinking button if suppression was made but is not yet processed by server.
 		$info_icons[] = makeSuppressedProblemIcon([[
 			'suppress_until' => $suppression_action['suppress_until'],
 			'username' => array_key_exists($suppression_action['userid'], $actions['users'])
 				? getUserFullname($actions['users'][$suppression_action['userid']])
 				: _('Inaccessible user')
 		]], true);
 	}
}

$opdata = null;
if ($show_opdata != OPERATIONAL_DATA_SHOW_NONE) {

 	if ($trigger['opdata'] === '') {
 		if ($show_opdata == OPERATIONAL_DATA_SHOW_SEPARATELY) {
 			$opdata = (new CCol(CScreenProblem::getLatestValues($trigger['items'])))->addClass('latest-values');
 		}
 	}	else {
 		$opdata = CMacrosResolverHelper::resolveTriggerOpdata(
 			[
 				'triggerid' => $trigger['triggerid'],
 				'expression' => $trigger['expression'],
 				'opdata' => $trigger['opdata'],
 				'clock' => $problem['clock'],
 				'ns' => $problem['ns']
 			],
 			[
 				'events' => true,
 				'html' => true
 			]
 		);

 		if ($show_opdata == OPERATIONAL_DATA_SHOW_SEPARATELY) {
 			$opdata = (new CCol($opdata))
 				->addClass('opdata')
 				->addClass(ZBX_STYLE_WORDWRAP);
 		}
 	}
}

$can_be_closed = ($trigger['manual_close'] == ZBX_TRIGGER_MANUAL_CLOSE_ALLOWED && $data['allowed']['close']);

if ($problem['r_eventid'] != 0) {
 	$can_be_closed = false;
} else {
	foreach ($problem['acknowledges'] as $acknowledge) {
 		if (($acknowledge['action'] & ZBX_PROBLEM_UPDATE_CLOSE) == ZBX_PROBLEM_UPDATE_CLOSE) {
 			$can_be_closed = false;
 			break;
 		}
 	}
}

// Create acknowledge link.
$is_acknowledged = ($problem['acknowledged'] == EVENT_ACKNOWLEDGED);
$problem_update_link = ($data['allowed']['add_comments'] || $data['allowed']['change_severity'] || $data['allowed']['acknowledge']
 		|| $can_be_closed || $data['allowed']['suppress'])
 	? (new CLink(_('Update')))
 		->addClass(ZBX_STYLE_LINK_ALT)
 		->setAttribute('data-eventid', $problem['eventid'])
 		->onClick('acknowledgePopUp({eventids: [this.dataset.eventid]}, this);')
 	: new CSpan(_('Update'));

$table->addRow(array_merge($row, [
 	makeInformationList($info_icons),
 	$triggers_hosts[$trigger['triggerid']],
 	CSeverityHelper::makeSeverityCell((int) $problem['severity'],
 		(($show_opdata == OPERATIONAL_DATA_SHOW_WITH_PROBLEM && $opdata)
 			? [$problem['name'], ' (', $opdata, ')']
 			: $problem['name']
 		)
 	)->addClass(ZBX_STYLE_WORDBREAK),
 	($show_opdata == OPERATIONAL_DATA_SHOW_SEPARATELY) ? $opdata : null,
 	zbx_date2age($problem['clock']),
 	$problem_update_link,
 	makeEventActionsIcons($problem['eventid'], $data['actions']['all_actions'], $data['actions']['users'], $is_acknowledged),
 	($show_tags ) ? $tags[$problem['eventid']]: null
 ]));
}

$table->addClass(ZBX_STYLE_NOWRAP);

$output = [
	'header' => "Problems",
	'body' => $table->toString(),
    'buttons' => [	]
];

echo json_encode($output);