<?php

declare(strict_types=0);
/*
** Glaber
** Copyright (C) 2001-2024 Glaber
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
**/

$content = new CDiv();

$url_table = createUrlTable($data);

$form = (new CForm('ruleedit'))
	->addItem((new CVar(CCsrfTokenHelper::CSRF_TOKEN_NAME, CCsrfTokenHelper::get('acknowledge')))->removeId())
	->addVar('dashboardid', $data['dashboardid'])
	->addVar('widgetid', $data['widgetid'])
	->addVar('elementid', $data['elementid']);

$colorizeSelectObjects = createObjectsTable($data, $form);

$edit_form = (new CFormGrid())
	->addItem([
		(new CLabel(_('Bind object(s)'), 'bind_object_type')),
		(new CFormField())
			->addClass(ZBX_STYLE_TABLE_FORMS_SEPARATOR)
			->addStyle('display:block;')
			->addItem( 
				(new CRadioButtonList('bind_object_type', (int) $data['fields']['bind_object_type']))
					->addValue(_('None'), 0)
					->addValue(_('Hosts'), OBJECT_TYPE_HOST)
					->addValue(_('Host Groups'), OBJECT_TYPE_HOSTGROUP)
					->addValue(_('Items'), OBJECT_TYPE_ITEM)
					->addValue(_('Triggers'), OBJECT_TYPE_TRIGGER)
					->setModern(true)
			)
			->addItem($colorizeSelectObjects)
	])
	->addItem([(new CLabel(_('Colorize by problem(s)'), 'colorize_type'))->addClass('colorize_type'),	
		(new CFormField())
			->addClass(ZBX_STYLE_TABLE_FORMS_SEPARATOR)
			->addClass('colorize_type')
			->addStyle('display:block;')
			->addItem( 
				(new CRadioButtonList('colorize_type', (int) $data['fields']['colorize_type']))
					->addValue(_('None'), COLORIZE_MODE_NONE)
					->addValue(_('Fill'), COLORIZE_MODE_FILL)
					->addValue(_('Outline'), COLORIZE_MODE_OUTLINE)
					->addValue(_('Text'), COLORIZE_MODE_TEXT)
					->setModern(true)
					->addClass('colorize_type')
	)])
	->addItem([(new CLabel(_('On click action'), 'on_click_action_type')),	
			(new CFormField())
				->addClass(ZBX_STYLE_TABLE_FORMS_SEPARATOR)
				->addStyle('display:block;')
				->addItem( 
					(new CRadioButtonList('on_click_action_type', (int) $data['fields']['on_click_action_type']))
						->addValue(_('None'), ONCLICK_ACTION_NONE)
						->addValue(_('Custom URL menu'), ONCLICK_ACTION_URL_MENU)
						->addValue(_('Object(s) view'), ONCLICK_ACTION_OBJECT_VIEW)
//						->addValue(_('Problems list view'), ONCLICK_ACTION_PROBLEMS_LIST) //uncomment when support will be added
						->setModern(true)
	)])
	->addItem([
		(new CLabel(_('URLs'), 'urls'))->addClass('url_table'),
		(new CFormField())
			->addItem($url_table)
			->addClass('url_table')
	]);


$form->addItem($edit_form);

//new url form template
$content
	->addItem(

		(new CScriptTag(

			(new CRow([
				(new CTextBox('urls[#{id}][name]'))->setWidth(ZBX_TEXTAREA_SMALL_WIDTH),
				(new CTextBox('urls[#{id}][url]'))->setWidth(ZBX_TEXTAREA_STANDARD_WIDTH)
					->setAttribute('maxlength', 2048),
				(new CCol(
					(new CButton(null, _('Remove')))
						->onClick('$("#url-row-#{id}").remove();')
						->addClass(ZBX_STYLE_BTN_LINK)
				))->addClass(ZBX_STYLE_NOWRAP)
			]))->setId('url-row-#{id}')
		)
		)->setAttribute('type', "text/x-jquery-tmpl")
			->setAttribute('id', "url-tpl")
	)
	->addItem($form);


$output = [
	'header' => "Edit object rule",
	'body' => $content->toString(),
	'buttons' => [
		[
			'title' => _('Update'),
			'class' => 'js-update',
			'keepOpen' => true,
			'isSubmit' => true,
			'action' => 'widget_rule_edit.submitRule(overlay);'
		],
	],

	'script_inline' => getPagePostJs() . $this->readJsFile('widget.rule.edit.js.php') .
		'widget_rule_edit.init();'
];

echo json_encode($output);

function createObjectsTable(array $data, $form) {
	$table =(new CTable())
		->addItem((new CRow([
	   		new CCol(new CLabel(_('Hosts'),'hostids[]')),
	   			new CCol((new CMultiSelect([
				   'name' => 'hostids[]',
				   'object_name' => 'hosts',
				   'data' => $data['fields']['hosts'] ? $data['fields']['hosts'] : [],
				   'multiple' => true,
				   'popup' => [
				   'parameters' => [
					   'srctbl' => 'hosts',
					   'srcfld1' => 'hostid',
					   'dstfrm' => $form->getName(),
					   'dstfld1' => 'hostids_',
				   ]]
			   ]))->setWidth(ZBX_TEXTAREA_FILTER_STANDARD_WIDTH))
	   		]))->addClass('host_fields'))
   		->addItem((new CRow([
	   		new CCol(new CLabel(_('Host groups'), 'groupids[]')),
	   			new CCol((new CMultiSelect([
				   'name' => 'groupids[]',
				   'object_name' => 'hostGroup',
				   'data' => $data['fields']['groups'] ? $data['fields']['groups'] : [],
				   'multiple' => true,
				   'popup' => [
					   'parameters' => [
						   'srctbl' => 'host_groups',
						   'srcfld1' => 'groupid',
						   'dstfrm' => $form->getName(),
						   'dstfld1' => 'groupids_',
						   'with_hosts' => true,
						   'multiselect' => '1'
					   ]
				   ]]))->setWidth(ZBX_TEXTAREA_FILTER_STANDARD_WIDTH)
				   	   ->setAriaRequired()
			   )]))->addClass('group_fields'))
   		->addItem((new CRow([
	   		new CCol(new CLabel(_('Triggers'), 'triggerids')),
	   			new CCol((new CMultiSelect([
		   			'name' => 'triggerids[]',
		   			'object_name' => 'triggers',
		   			'data' => $data['fields']['triggers'] ? $data['fields']['triggers'] : [],
		   			'multiple' => true,
		   			'popup' => [
			   			'parameters' => [
				   		'srctbl' => 'triggers',
				   		'srcfld1' => 'triggerid',
				   		'dstfrm' => $form->getName(),
				   		'dstfld1' => 'triggerids_',
			   		]]
	   			]))->setWidth(ZBX_TEXTAREA_FILTER_STANDARD_WIDTH)
   			)]))->addClass('trigger_fields'))
   		->addItem((new CRow([
	   		new CCol(new CLabel(_('Items'), 'itemids')),
	   			new CCol((new CMultiSelect([
		   			'name' => 'itemids[]',
		   			'object_name' => 'items',
		   			'data' => $data['fields']['items'] ? $data['fields']['items'] : [],
		   			'multiple' => true,
		   			'popup' => [
			   			'parameters' => [
				   			'srctbl' => 'items',
				   			'srcfld1' => 'itemid',
				   			'srcfld2' => 'name',
				   			'dstfrm' => $form->getName(),
				   			'dstfld1' => 'itemids_',
				   			'numeric' => '1',
				   			'real_hosts' => true //!$data['is_template']
			   		]]
	   			]))->setWidth(ZBX_TEXTAREA_FILTER_STANDARD_WIDTH)
   			)]))->addClass('item_fields'));
	return $table;
}

function createUrlTable(array &$data)
{
	$url_table = (new CTable())
		->setAttribute('style', 'width: 100%;')
		->addClass('table-forms-separator')
		->addRow([_('Name'), _('URL'),  _('Action')]);

	if (empty($data['fields']['urls']))
		$data['urls'][] = ['name' => '', 'url' => ''];

	$i = 0;

	if (isset($data['fields']['urls']))

		foreach ($data['fields']['urls'] as $url) {

			$url_table->addRow(
				(new CRow([
					(new CTextBox('urls[' . $i . '][name]', $url['name']))->setWidth(ZBX_TEXTAREA_SMALL_WIDTH),
					(new CTextBox('urls[' . $i . '][url]', $url['url']))->setWidth(ZBX_TEXTAREA_STANDARD_WIDTH)->setAttribute('maxlength', 2048),
					(new CCol(
						(new CButton(null, _('Remove')))
							->onClick('$("#url-row-' . $i . '").remove();')
							->addClass(ZBX_STYLE_BTN_LINK)
					))->addClass(ZBX_STYLE_NOWRAP)
				]))->setId('url-row-' . $i)
			);
			$i++;
		}

	$url_table->addRow(
		(new CCol(
			(new CButton(null, _('Add')))
				->setId('add-url')
				->addClass(ZBX_STYLE_BTN_LINK)
		)
		)->setColSpan(3)
	);

	return $url_table;
}
