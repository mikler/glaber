<?php
/*
** Glaber
** Copyright (C) 2001-2024 Glaber JSC
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**/


/**
 * @var CView $this
 */
?>
window.widget_rule_edit = new class {
	init() {

		document.getElementById('bind_object_type').addEventListener('change', () => { this._update(); });
		document.getElementById('on_click_action_type').addEventListener('change', () => {this._update();});
		
		this._update();

	}

	_set_visibility(className, visible) {
  		const elements = document.getElementsByClassName(className);
  		for (const e of elements) {
   			visible? e.style.display = 'block' : e.style.display = 'none';
  		}
	}

	_update() {
		const $bind_type = $('input[name=bind_object_type]:checked').val();
		const $on_click_type = $('input[name=on_click_action_type]:checked').val();
		
		this._set_visibility('host_fields', $bind_type == 1);
		this._set_visibility('group_fields', $bind_type == 2);
		this._set_visibility('item_fields', $bind_type == 3);
		this._set_visibility('trigger_fields', $bind_type == 4);
		this._set_visibility('colorize_type', $bind_type != 0);

		this._set_visibility('url_table', $on_click_type == 1);
		
	}

	/**
	 * @param {Overlay} overlay
	 */
	submitRule(overlay) {
		var $form = overlay.$dialogue.find('form'),
			url = new Curl('zabbix.php'),
			form_data;

		console.log($form);

		//$form.trimValues(['#message', '#suppress_until_problem']);
		form_data = jQuery('#message, input:visible, input[type=hidden]', $form).serialize();
		url.setArgument('action', 'widget.svgmap.rule.update');

		overlay.xhr = sendAjaxData(url.getUrl(), {
			data: form_data,
			dataType: 'json',
			method: 'POST',
			beforeSend: function() {
				overlay.setLoading();
			},
			complete: function() {
				overlay.unsetLoading();
				overlayDialogueDestroy(overlay.dialogueid);
			}
		})
		.done(function(response) {
			console.log("Closing the form123");
			overlay.$dialogue.find('.msg-bad').remove();

			if ('error' in response) {
				const message_box = makeMessageBox('bad', response.error.messages,
					response.error.title
				);

				message_box.insertBefore($form);
			}
			else {
				overlayDialogueDestroy(overlay.dialogueid);
				$.publish('acknowledge.create', [response, overlay]);
			}
		});
	}
};

jQuery(function($) {
	const url_tpl = new Template($('#url-tpl').html());
	const $add_url_btn = $('#add-url');
	const $add_url_row = $add_url_btn.closest('tr');
	let url_rowid = $('[id^="url-row-"]').length;

	$add_url_btn.on('click', (e) => {
		$add_url_row.before(url_tpl.evaluate({id: url_rowid}));
		url_rowid++;
	});
});