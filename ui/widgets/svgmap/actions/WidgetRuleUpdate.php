<?php declare(strict_types = 0);
/*
** Glaber
** Copyright (C) 2001-2024 Glaber JSC
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**/
namespace Widgets\SvgMap\Actions;
require_once __DIR__ . '/../includes/WidgetFieldUpdater.php';

use API, APP, CControllerResponseData,WidgetFieldUpdater;

class WidgetRuleUpdate extends \CController {

	protected function init() {
		$this->disableCSRFValidation();
	}

	protected function checkPermissions(): bool {
		return $this->getUserType() >= USER_TYPE_ZABBIX_USER;
	}

	protected function checkInput(): bool {
        $fields = [
			'dashboardid' => '',
			'widgetid' => ''
		];

		return	 $this->validateInput($fields);
	}
    protected function setFields(array $fields_names) {
        $fields = [];
        $raw_input = $this->getRawInput();

        foreach ($fields_names as $field_name) {
            if (isset($raw_input[$field_name]))
                $fields[$field_name] = $raw_input[$field_name];
        }

        return $fields;
    }

	protected function doAction(): void {
        $raw_input = $this->getRawInput();
        $new_rules = [];
        $fields = [];

        $dashboardid = $raw_input['dashboardid'];
        $widgetid = $raw_input['widgetid'];
        $elementid = $raw_input['elementid'];
        
        $fields = $this->setFields(['bind_object_type', 'hostids', 
                            'itemids', 'groupids', 'triggerids', 'urls', 'colorize_type', 'on_click_action_type'
                        ]);
        
        try {
            $widget = new WidgetFieldUpdater($dashboardid, $widgetid);
        } catch (Exception $e) {
            $this->setResponse(new CControllerResponseFatal());
            return;
        }

        $widget->setActionsRuleForId($elementid, $fields);
        $widget->Save();

		$this->setResponse(new CControllerResponseData([
            'name' => "The list of the hosts",
            'content' => "test",
            'fields' => $fields
		]));
	}
}