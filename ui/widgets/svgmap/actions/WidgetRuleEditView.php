<?php declare(strict_types = 0);
/*
** Copyright (C) 2001-2024 Glaber JSC
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**/
namespace Widgets\SvgMap\Actions;
require_once __DIR__ . '/../includes/WidgetFieldUpdater.php';

use API, APP, CControllerResponseData,
	CRoleHelper, CWebUser, WidgetFieldUpdater, CArrayHelper;

class WidgetRuleEditView extends \CController {

	protected $fields;

	protected function init() {
		$this->disableCSRFValidation();
	}

	protected function checkPermissions(): bool {
		return $this->getUserType() >= USER_TYPE_ZABBIX_USER;
	}

	protected function checkInput(): bool {
        $fields = [
			'elementid' =>					'string',
			'dashboardid' =>	'string',
			'widgetid' =>	'string'
		];


		return	 $this->validateInput($fields);
	}

	protected function doAction(): void {
		$widgetid = $this->getInput('widgetid');
		$dashboardid = $this->getInput('dashboardid');
		$elementid =  $this->getInput('elementid');
		
		try {
            $widget = new WidgetFieldUpdater($dashboardid, $widgetid);
        } catch (Exception $e) {
            $this->setResponse(new CControllerResponseFatal());
            return;
        }
	
		$this->fields = $widget->fetchActionsRuleForId($elementid);

		$this->ExtractColorizeMultiselectData();

		$this->setResponse(new CControllerResponseData([
			'name' => "The list of the hosts",
            'content' => "",
			'fields' => $this->fields,
			'dashboardid' => $dashboardid, 
			'widgetid' => $widgetid, 
			'elementid' => $elementid
		]));
	}
	
	private function ExtractColorizeMultiselectData() {
		if (isset($this->fields['bind_object_type']) && $this->fields['bind_object_type'] > 0) {
			$this->fields['hosts'] = [];
			$this->fields['items'] = [];
			$this->fields['triggers'] = [];
			$this->fields['groups'] = [];

			switch ($this->fields['bind_object_type']) {
				case OBJECT_TYPE_HOST: 
					if (isset( $this->fields['hostids'])) {
					
						$filter_hosts = API::Host()->get([
							'output' => ['hostid', 'name'],
							'hostids' => $this->fields['hostids'],
							'preservekeys' => true
						]);
						
						if (isset($filter_hosts) && is_array($filter_hosts))
							$this->fields['hosts'] = CArrayHelper::renameObjectsKeys($filter_hosts, ['hostid' => 'id']);
					}
				case OBJECT_TYPE_HOSTGROUP:
					if (isset( $this->fields['groupids'])) {
						$filter_groups = API::HostGroup()->get([
							'output' => ['groupid', 'name'],
							'groupids' => $this->fields['groupids'],
							'preservekeys' => true
						]);
						
						if (isset($filter_groups) && is_array($filter_groups))
							$this->fields['groups'] = CArrayHelper::renameObjectsKeys($filter_groups, ['groupid' => 'id']);
					}
					break;
				case OBJECT_TYPE_ITEM:
					if (isset( $this->fields['itemids'])) {
						$filter_items = API::Item()->get([
							'output' => ['itemid', 'name'],
							'itemids' => $this->fields['itemids'],
							'preservekeys' => true
						]);
						
						if (isset($filter_items) && is_array($filter_items))
							$this->fields['items'] = CArrayHelper::renameObjectsKeys($filter_items, ['itemid' => 'id']);
					}
					break;
				case OBJECT_TYPE_TRIGGER:
					if (isset($this->fields['triggerids'])) {
						$filter_triggers = API::Trigger()->get([
							'output' => ['triggerid', 'description'],
							'triggerids' => $this->fields['triggerids'],
							'preservekeys' => true
						]);
					
						if (isset($filter_triggers) && is_array($filter_triggers))
							$this->fields['triggers'] = CArrayHelper::renameObjectsKeys($filter_triggers, ['triggerid' => 'id', 'description' => 'name']);
					}
					break;
			}
		} 
	}
}
