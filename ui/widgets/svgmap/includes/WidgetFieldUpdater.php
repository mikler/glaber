<?php declare(strict_types = 0);
/*
** Glaber
** Copyright (C) 2001-2024 Glaber JSC
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**/

class WidgetFieldUpdater {

    protected $dashboardid;
    protected $widgetid;
    protected $dashboardData;
    protected $widget;

    public function __construct( $dashboardid, $widgetid) {
        $this->dashboardid = $dashboardid;
        $this->widgetid = $widgetid;
        $this->dashboardData = API::Dashboard()->get([
			'output' => API_OUTPUT_EXTEND,
			'dashboardids' => [$dashboardid],
			'editable' => true,
            'selectPages' => ['dashboard_pageid', 'widgets']
		]);

        if (!isset($this->dashboardData) || !is_array($this->dashboardData) || count($this->dashboardData) < 1)
            throw new Exception("Couldn't find dashboard by id $dashboardid");

        //need to remove uuid for updating, API complains
        if (isset($this->dashboardData[0]['uuid']))
                unset($this->dashboardData[0]['uuid']);

        foreach ($this->dashboardData[0]['pages'] as $idx => &$page ) {
            foreach ($page['widgets'] as $idx =>&$widget )
                if ($widget['widgetid'] == $widgetid)
                    $this->widget = &$widget;
        }
    }

    public function GetFieldValue($field_name) {
        
        foreach($this->widget['fields'] as $idx => &$field) 
            if ($field['name'] == $field_name) 
                return $field['value'];
        
        return '';
    }
    
    public function SetFieldValue($field_name, $new_value, $default_field_type = '1') {

        foreach($this->widget['fields'] as $idx => &$field) {
             
            if ($field['name'] == $field_name) {
                $field['value'] = $new_value;
                return;
            }
        }

        $this->widget['fields'][] = ["type" => $default_field_type, "name"=> $field_name, "value" => $new_value];   
    }

    public function fetchActionsRuleForId($elementid) {
		
		$actions_json = $this->GetFieldValue('actions');
		
		if (!$actions_json) 
			$actions_json = '{}';

		$actions = json_decode($actions_json, true);
		
		if ($actions === null && json_last_error() !== JSON_ERROR_NONE) {
			throw new \Exception("Cannot parse actions settings, not a valid JSON");
		}
		

		if (isset($actions[$elementid])) 
			$rules = $actions[$elementid];
		else 
			$rules = $this->getDefaultRules();

		return $rules;
	}

    public function setActionsRuleForId($elementid, $rules) {
        
        $actions_json = $this->GetFieldValue('actions');

        if (!$actions_json) 
            $actions_json = '{}';
        
        $actions = json_decode($actions_json, true);
                
        if ($actions === null && json_last_error() !== JSON_ERROR_NONE) {
            throw new \Exception("Cannot parse actions settings, not a valid JSON");
        }
                
        $actions[$elementid] = $rules;
    
        $this->SetFieldValue('actions', json_encode($actions), '1');
    }

    public function Save() {
        API::Dashboard()->update($this->dashboardData);
    }

    protected function getDefaultRules() {
		return [
			'bind_object_type' => 0,
            'data_object_type' => 0,
            'colorize_type' => COLORIZE_MODE_FILL,
            'on_click_action_type' => ONCLICK_ACTION_URL_MENU,
            'dtriggerid' => 0, 
            'dhostid' => 0, 
            'ditemid' => 0,
            'dgroupid' => 0,
            'triggers' => [],
            'hosts' => [],
            'items' => [],
            'groups' => []
		];
	}
}