<?php declare(strict_types = 0);
/*
** Glaber
** Copyright (C) 2001-2024 Glaber JSC
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**/

class CControllerLatestItemDetails extends CController {
	
	protected function init() {
		$this->disableCSRFValidation();
	}

	protected function checkPermissions(): bool {
		return $this->getUserType() >= USER_TYPE_ZABBIX_USER;
	}

	protected function checkInput(): bool {
			$fields = [
			'itemids' => 'array'
		];
		
		return	 $this->validateInput($fields);
	}

	protected function doAction(): void {
	
		$itemids = $this->getInput('itemids', []);
	
        $items = API::Item()->get([
			'output' => ['itemid', 'type', 'hostid', 'host', 'name', 'key_', 'delay', 'history', 'trends', 'status',
				'value_type', 'units', 'description', 'state', 'error', 'status'],
			'selectHosts' => ['hostid','host','name'],
			'selectTags' => ['tag', 'value'],
			'selectValueMap' => ['mappings'],
            'itemids' => $itemids,
			'webitems' => true,
			'preservekeys' => true,
			'discovery_items' => true,
			'selectTriggers' => ['triggerid', 'name', 'value', 'priority']
			]);
        
        $history = Manager::History()->getLastValues($itemids, 2,
            timeUnitToSeconds(CSettingsHelper::get(CSettingsHelper::HISTORY_PERIOD))
        );

		$this->setResponse(new CControllerResponseData([
			'name' => "Item details",
            'items' => $items, //[$itemids[0]], 
			'itemids' => $itemids,
            'history' => $history, //isset($history[$itemids[0]])? $history[$itemids[0]]: []
        ]));
	}
    
}
