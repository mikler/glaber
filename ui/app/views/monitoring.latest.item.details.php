<?php declare(strict_types = 0);
/*
** Glaber
** Copyright (C) 2001-2024 Glaber JSC
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**/

$itemid = $data['itemids'][0];

$output = [
	'header' => $data['items'][$itemid]['name'],
	'body' => makeItemDetail($data['items'][$itemid], $data['history'][$itemid]),
    'buttons' => []
];

echo json_encode($output);

function makeItemDetail($item, $history) {
	$latestItem = new CLatestValue($item, $history, null, 1);
	$latestItem->makeHint();
	return $latestItem->hintbox->toString();
}