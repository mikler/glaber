CREATE DATABASE glaber ON CLUSTER glaber_cluster;

-- history_dbl_local
CREATE TABLE
    glaber.history_dbl_local ON CLUSTER glaber_cluster (
        day Date,
        itemid UInt64,
        clock DateTime,
        hostname String,
        itemname String,
        ns UInt32,
        value Float64
    ) ENGINE = ReplicatedMergeTree (
        '/glaber_cluster/tables/{shard}/glaber.history_dbl_local',
        '{replica}'
    )
PARTITION BY
    toYYYYMM (day)
ORDER BY
    (itemid, clock) TTL day + INTERVAL 6 MONTH DELETE;

-- history_dbl_global
CREATE TABLE
    glaber.history_dbl ON CLUSTER glaber_cluster (
        day Date,
        itemid UInt64,
        clock DateTime,
        hostname String,
        itemname String,
        ns UInt32,
        value Float64
    ) ENGINE = Distributed (
        '{cluster}',
        'glaber',
        'history_dbl_local',
        itemid
    );

-- history_uint_local
CREATE TABLE
    glaber.history_uint_local ON CLUSTER glaber_cluster (
        day Date,
        itemid UInt64,
        clock DateTime,
        hostname String,
        itemname String,
        ns UInt32,
        value UInt64
    ) ENGINE = ReplicatedMergeTree (
        '/glaber_cluster/tables/{shard}/glaber.history_uint_local',
        '{replica}'
    )
PARTITION BY
    toYYYYMM (day)
ORDER BY
    (itemid, clock) TTL day + INTERVAL 6 MONTH DELETE;

-- history_uint_global
CREATE TABLE
    glaber.history_uint ON CLUSTER glaber_cluster (
        day Date,
        itemid UInt64,
        clock DateTime,
        hostname String,
        itemname String,
        ns UInt32,
        value UInt64
    ) ENGINE = Distributed (
        '{cluster}',
        'glaber',
        'history_uint_local',
        itemid
    );

-- glaber.history_str_local
CREATE TABLE
    glaber.history_str_local ON CLUSTER glaber_cluster (
        day Date,
        itemid UInt64,
        clock DateTime,
        hostname String,
        itemname String,
        ns UInt32,
        value String
    ) ENGINE = ReplicatedMergeTree (
        '/glaber_cluster/tables/{shard}/glaber.history_str_local',
        '{replica}'
    )
PARTITION BY
    toYYYYMM (day)
ORDER BY
    (itemid, clock) TTL day + INTERVAL 6 MONTH DELETE;

-- glaber.history_str_global
CREATE TABLE
    glaber.history_str ON CLUSTER glaber_cluster (
        day Date,
        itemid UInt64,
        clock DateTime,
        hostname String,
        itemname String,
        ns UInt32,
        value String
    ) ENGINE = Distributed (
        '{cluster}',
        'glaber',
        'history_str_local',
        itemid
    );

--history_log_local
CREATE TABLE
    glaber.history_log_local ON CLUSTER glaber_cluster (
        day Date,
        itemid UInt64,
        clock DateTime,
        logeventid UInt64,
        source String,
        severity UInt8,
        hostname String,
        itemname String,
        ns UInt32,
        value String
    ) ENGINE = ReplicatedMergeTree (
        '/glaber_cluster/tables/{shard}/glaber.history_log_local',
        '{replica}'
    )
PARTITION BY
    toYYYYMM (day)
ORDER BY
    (itemid, clock) TTL day + INTERVAL 6 MONTH DELETE;

--history_log_global
CREATE TABLE
    glaber.history_log ON CLUSTER glaber_cluster (
        day Date,CREATE DATABASE glaber ON CLUSTER glaber_cluster;

-- history_dbl_local
CREATE TABLE
    glaber.history_dbl_local ON CLUSTER glaber_cluster (
        day Date,
        itemid UInt64,
        clock DateTime,
        hostname String,
        itemname String,
        ns UInt32,
        value Float64
    ) ENGINE = ReplicatedMergeTree (
        '/glaber_cluster/tables/{shard}/glaber.history_dbl_local',
        '{replica}'
    )
PARTITION BY
    toYYYYMM (day)
ORDER BY
    (itemid, clock) TTL day + INTERVAL 6 MONTH DELETE;

-- history_dbl_global
CREATE TABLE
    glaber.history_dbl ON CLUSTER glaber_cluster (
        day Date,
        itemid UInt64,
        clock DateTime,
        hostname String,
        itemname String,
        ns UInt32,
        value Float64
    ) ENGINE = Distributed (
        '{cluster}',
        'glaber',
        'history_dbl_local',
        itemid
    );

-- history_uint_local
CREATE TABLE
    glaber.history_uint_local ON CLUSTER glaber_cluster (
        day Date,
        itemid UInt64,
        clock DateTime,
        hostname String,
        itemname String,
        ns UInt32,
        value UInt64
    ) ENGINE = ReplicatedMergeTree (
        '/glaber_cluster/tables/{shard}/glaber.history_uint_local',
        '{replica}'
    )
PARTITION BY
    toYYYYMM (day)
ORDER BY
    (itemid, clock) TTL day + INTERVAL 6 MONTH DELETE;

-- history_uint_global
CREATE TABLE
    glaber.history_uint ON CLUSTER glaber_cluster (
        day Date,
        itemid UInt64,
        clock DateTime,
        hostname String,
        itemname String,
        ns UInt32,
        value UInt64
    ) ENGINE = Distributed (
        '{cluster}',
        'glaber',
        'history_uint_local',
        itemid
    );

-- glaber.history_str_local
CREATE TABLE
    glaber.history_str_local ON CLUSTER glaber_cluster (
        day Date,
        itemid UInt64,
        clock DateTime,
        hostname String,
        itemname String,
        ns UInt32,
        value String
    ) ENGINE = ReplicatedMergeTree (
        '/glaber_cluster/tables/{shard}/glaber.history_str_local',
        '{replica}'
    )
PARTITION BY
    toYYYYMM (day)
ORDER BY
    (itemid, clock) TTL day + INTERVAL 6 MONTH DELETE;

-- glaber.history_str_global
CREATE TABLE
    glaber.history_str ON CLUSTER glaber_cluster (
        day Date,
        itemid UInt64,
        clock DateTime,
        hostname String,
        itemname String,
        ns UInt32,
        value String
    ) ENGINE = Distributed (
        '{cluster}',
        'glaber',
        'history_str_local',
        itemid
    );

--history_log_local
CREATE TABLE
    glaber.history_log_local ON CLUSTER glaber_cluster (
        day Date,
        itemid UInt64,
        clock DateTime,
        logeventid UInt64,
        source String,
        severity UInt8,
        hostname String,
        itemname String,
        ns UInt32,
        value String
    ) ENGINE = ReplicatedMergeTree (
        '/glaber_cluster/tables/{shard}/glaber.history_log_local',
        '{replica}'
    )
PARTITION BY
    toYYYYMM (day)
ORDER BY
    (itemid, clock) TTL day + INTERVAL 6 MONTH DELETE;

--history_log_global
CREATE TABLE
    glaber.history_log ON CLUSTER glaber_cluster (
        day Date,
        itemid UInt64,
        clock DateTime,
        logeventid UInt64,
        source String,
        severity UInt8,
        hostname String,
        itemname String,
        ns UInt32,
        value String
    ) ENGINE = Distributed (
        '{cluster}',
        'glaber',
        'history_log_local',
        itemid
    );

--trends_dbl_local
CREATE TABLE
    glaber.trends_dbl_local ON CLUSTER glaber_cluster (
        day Date,
        itemid UInt64,
        clock DateTime,
        value_min Float64,
        value_max Float64,
        value_avg Float64,
        count UInt32,
        hostname String,
        itemname String
    ) ENGINE = ReplicatedMergeTree (
        '/glaber_cluster/tables/{shard}/glaber.trends_dbl_local',
        '{replica}'
    )
PARTITION BY
    toYYYYMM (day)
ORDER BY
    (itemid, clock) TTL day + toIntervalMonth (24);

--trends_dbl_global
CREATE TABLE
    glaber.trends_dbl ON CLUSTER glaber_cluster (
        day Date,
        itemid UInt64,
        clock DateTime,
        value_min Float64,
        value_max Float64,
        value_avg Float64,
        count UInt32,
        hostname String,
        itemname String
    ) ENGINE = Distributed ('{cluster}', 'glaber', 'trends_dbl_local', itemid);

--trends_uint_local
CREATE TABLE
    glaber.trends_uint_local ON CLUSTER glaber_cluster (
        day Date,
        itemid UInt64,
        clock DateTime,
        value_min Int64,
        value_max Int64,
        value_avg Int64,
        count UInt32,
        hostname String,
        itemname String
    ) ENGINE = ReplicatedMergeTree (
        '/glaber_cluster/tables/{shard}/glaber.trends_uint_local',
        '{replica}'
    )
PARTITION BY
    toYYYYMM (day)
ORDER BY
    (itemid, clock) TTL day + toIntervalMonth (24);

--trends_uint_global
CREATE TABLE
    glaber.trends_uint ON CLUSTER glaber_cluster (
        day Date,
        itemid UInt64,
        clock DateTime,CREATE DATABASE glaber ON CLUSTER glaber_cluster;

-- history_dbl_local
CREATE TABLE
    glaber.history_dbl_local ON CLUSTER glaber_cluster (
        day Date,
        itemid UInt64,
        clock DateTime,
        hostname String,
        itemname String,
        ns UInt32,
        value Float64
    ) ENGINE = ReplicatedMergeTree (
        '/glaber_cluster/tables/{shard}/glaber.history_dbl_local',
        '{replica}'
    )
PARTITION BY
    toYYYYMM (day)
ORDER BY
    (itemid, clock) TTL day + INTERVAL 6 MONTH DELETE;

-- history_dbl_global
CREATE TABLE
    glaber.history_dbl ON CLUSTER glaber_cluster (
        day Date,
        itemid UInt64,
        clock DateTime,
        hostname String,
        itemname String,
        ns UInt32,
        value Float64
    ) ENGINE = Distributed (
        '{cluster}',
        'glaber',
        'history_dbl_local',
        itemid
    );

-- history_uint_local
CREATE TABLE
    glaber.history_uint_local ON CLUSTER glaber_cluster (
        day Date,
        itemid UInt64,
        clock DateTime,
        hostname String,
        itemname String,
        ns UInt32,
        value UInt64
    ) ENGINE = ReplicatedMergeTree (
        '/glaber_cluster/tables/{shard}/glaber.history_uint_local',
        '{replica}'
    )
PARTITION BY
    toYYYYMM (day)
ORDER BY
    (itemid, clock) TTL day + INTERVAL 6 MONTH DELETE;

-- history_uint_global
CREATE TABLE
    glaber.history_uint ON CLUSTER glaber_cluster (
        day Date,
        itemid UInt64,
        clock DateTime,
        hostname String,
        itemname String,
        ns UInt32,
        value UInt64
    ) ENGINE = Distributed (
        '{cluster}',
        'glaber',
        'history_uint_local',
        itemid
    );

-- glaber.history_str_local
CREATE TABLE
    glaber.history_str_local ON CLUSTER glaber_cluster (
        day Date,
        itemid UInt64,
        clock DateTime,
        hostname String,
        itemname String,
        ns UInt32,
        value String
    ) ENGINE = ReplicatedMergeTree (
        '/glaber_cluster/tables/{shard}/glaber.history_str_local',
        '{replica}'
    )
PARTITION BY
    toYYYYMM (day)
ORDER BY
    (itemid, clock) TTL day + INTERVAL 6 MONTH DELETE;

-- glaber.history_str_global
CREATE TABLE
    glaber.history_str ON CLUSTER glaber_cluster (
        day Date,
        itemid UInt64,
        clock DateTime,
        hostname String,
        itemname String,
        ns UInt32,
        value String
    ) ENGINE = Distributed (
        '{cluster}',
        'glaber',
        'history_str_local',
        itemid
    );

--history_log_local
CREATE TABLE
    glaber.history_log_local ON CLUSTER glaber_cluster (
        day Date,
        itemid UInt64,
        clock DateTime,
        logeventid UInt64,
        source String,
        severity UInt8,
        hostname String,
        itemname String,
        ns UInt32,
        value String
    ) ENGINE = ReplicatedMergeTree (
        '/glaber_cluster/tables/{shard}/glaber.history_log_local',
        '{replica}'
    )
PARTITION BY
    toYYYYMM (day)
ORDER BY
    (itemid, clock) TTL day + INTERVAL 6 MONTH DELETE;

--history_log_global
CREATE TABLE
    glaber.history_log ON CLUSTER glaber_cluster (
        day Date,
        itemid UInt64,
        clock DateTime,
        logeventid UInt64,
        source String,
        severity UInt8,
        hostname String,
        itemname String,
        ns UInt32,
        value String
    ) ENGINE = Distributed (
        '{cluster}',
        'glaber',
        'history_log_local',
        itemid
    );

--trends_dbl_local
CREATE TABLE
    glaber.trends_dbl_local ON CLUSTER glaber_cluster (
        day Date,
        itemid UInt64,
        clock DateTime,
        value_min Float64,
        value_max Float64,
        value_avg Float64,
        count UInt32,
        hostname String,
        itemname String
    ) ENGINE = ReplicatedMergeTree (
        '/glaber_cluster/tables/{shard}/glaber.trends_dbl_local',
        '{replica}'
    )
PARTITION BY
    toYYYYMM (day)
ORDER BY
    (itemid, clock) TTL day + toIntervalMonth (24);

--trends_dbl_global
CREATE TABLE
    glaber.trends_dbl ON CLUSTER glaber_cluster (
        day Date,
        itemid UInt64,
        clock DateTime,
        value_min Float64,
        value_max Float64,
        value_avg Float64,
        count UInt32,
        hostname String,
        itemname String
    ) ENGINE = Distributed ('{cluster}', 'glaber', 'trends_dbl_local', itemid);

--trends_uint_local
CREATE TABLE
    glaber.trends_uint_local ON CLUSTER glaber_cluster (
        day Date,
        itemid UInt64,
        clock DateTime,
        value_min Int64,
        value_max Int64,
        value_avg Int64,
        count UInt32,
        hostname String,
        itemname String
    ) ENGINE = ReplicatedMergeTree (
        '/glaber_cluster/tables/{shard}/glaber.trends_uint_local',
        '{replica}'
    )
PARTITION BY
    toYYYYMM (day)
ORDER BY
    (itemid, clock) TTL day + toIntervalMonth (24);

--trends_uint_global
CREATE TABLE
    glaber.trends_uint ON CLUSTER glaber_cluster (
        day Date,
        itemid UInt64,
        clock DateTime,CREATE DATABASE glaber ON CLUSTER glaber_cluster;

-- history_dbl_local
CREATE TABLE
    glaber.history_dbl_local ON CLUSTER glaber_cluster (
        day Date,
        itemid UInt64,
        clock DateTime,
        hostname String,
        itemname String,
        ns UInt32,
        value Float64
    ) ENGINE = ReplicatedMergeTree (
        '/glaber_cluster/tables/{shard}/glaber.history_dbl_local',
        '{replica}'
    )
PARTITION BY
    toYYYYMM (day)
ORDER BY
    (itemid, clock) TTL day + INTERVAL 6 MONTH DELETE;

-- history_dbl_global
CREATE TABLE
    glaber.history_dbl ON CLUSTER glaber_cluster (
        day Date,
        itemid UInt64,
        clock DateTime,
        hostname String,
        itemname String,
        ns UInt32,
        value Float64
    ) ENGINE = Distributed (
        '{cluster}',
        'glaber',
        'history_dbl_local',
        itemid
    );

-- history_uint_local
CREATE TABLE
    glaber.history_uint_local ON CLUSTER glaber_cluster (
        day Date,
        itemid UInt64,
        clock DateTime,
        hostname String,
        itemname String,
        ns UInt32,
        value UInt64
    ) ENGINE = ReplicatedMergeTree (
        '/glaber_cluster/tables/{shard}/glaber.history_uint_local',
        '{replica}'
    )
PARTITION BY
    toYYYYMM (day)
ORDER BY
    (itemid, clock) TTL day + INTERVAL 6 MONTH DELETE;

-- history_uint_global
CREATE TABLE
    glaber.history_uint ON CLUSTER glaber_cluster (
        day Date,
        itemid UInt64,
        clock DateTime,
        hostname String,
        itemname String,
        ns UInt32,
        value UInt64
    ) ENGINE = Distributed (
        '{cluster}',
        'glaber',
        'history_uint_local',
        itemid
    );

-- glaber.history_str_local
CREATE TABLE
    glaber.history_str_local ON CLUSTER glaber_cluster (
        day Date,
        itemid UInt64,
        clock DateTime,
        hostname String,
        itemname String,
        ns UInt32,
        value String
    ) ENGINE = ReplicatedMergeTree (
        '/glaber_cluster/tables/{shard}/glaber.history_str_local',
        '{replica}'
    )
PARTITION BY
    toYYYYMM (day)
ORDER BY
    (itemid, clock) TTL day + INTERVAL 6 MONTH DELETE;

-- glaber.history_str_global
CREATE TABLE
    glaber.history_str ON CLUSTER glaber_cluster (
        day Date,
        itemid UInt64,
        clock DateTime,
        hostname String,
        itemname String,
        ns UInt32,
        value String
    ) ENGINE = Distributed (
        '{cluster}',
        'glaber',
        'history_str_local',
        itemid
    );

--history_log_local
CREATE TABLE
    glaber.history_log_local ON CLUSTER glaber_cluster (
        day Date,
        itemid UInt64,
        clock DateTime,
        logeventid UInt64,
        source String,
        severity UInt8,
        hostname String,
        itemname String,
        ns UInt32,
        value String
    ) ENGINE = ReplicatedMergeTree (
        '/glaber_cluster/tables/{shard}/glaber.history_log_local',
        '{replica}'
    )
PARTITION BY
    toYYYYMM (day)
ORDER BY
    (itemid, clock) TTL day + INTERVAL 6 MONTH DELETE;

--history_log_global
CREATE TABLE
    glaber.history_log ON CLUSTER glaber_cluster (
        day Date,
        itemid UInt64,
        clock DateTime,
        logeventid UInt64,
        source String,
        severity UInt8,
        hostname String,
        itemname String,
        ns UInt32,
        value String
    ) ENGINE = Distributed (
        '{cluster}',
        'glaber',
        'history_log_local',
        itemid
    );

--trends_dbl_local
CREATE TABLE
    glaber.trends_dbl_local ON CLUSTER glaber_cluster (
        day Date,
        itemid UInt64,
        clock DateTime,
        value_min Float64,
        value_max Float64,
        value_avg Float64,
        count UInt32,
        hostname String,
        itemname String
    ) ENGINE = ReplicatedMergeTree (
        '/glaber_cluster/tables/{shard}/glaber.trends_dbl_local',
        '{replica}'
    )
PARTITION BY
    toYYYYMM (day)
ORDER BY
    (itemid, clock) TTL day + toIntervalMonth (24);

--trends_dbl_global
CREATE TABLE
    glaber.trends_dbl ON CLUSTER glaber_cluster (
        day Date,
        itemid UInt64,
        clock DateTime,
        value_min Float64,
        value_max Float64,
        value_avg Float64,
        count UInt32,
        hostname String,
        itemname String
    ) ENGINE = Distributed ('{cluster}', 'glaber', 'trends_dbl_local', itemid);

--trends_uint_local
CREATE TABLE
    glaber.trends_uint_local ON CLUSTER glaber_cluster (
        day Date,
        itemid UInt64,
        clock DateTime,
        value_min Int64,
        value_max Int64,
        value_avg Int64,
        count UInt32,
        hostname String,
        itemname String
    ) ENGINE = ReplicatedMergeTree (
        '/glaber_cluster/tables/{shard}/glaber.trends_uint_local',
        '{replica}'
    )
PARTITION BY
    toYYYYMM (day)
ORDER BY
    (itemid, clock) TTL day + toIntervalMonth (24);

--trends_uint_global
CREATE TABLE
    glaber.trends_uint ON CLUSTER glaber_cluster (
        day Date,
        itemid UInt64,
        clock DateTime,
        value_min Int64,
        value_max Int64,
        value_avg Int64,
        count UInt32,
        hostname String,
        itemname String
    ) ENGINE = Distributed (
        '{cluster}',
        'glaber',
        'trends_uint_local',
        itemid
    );
        value_min Int64,
        value_max Int64,
        value_avg Int64,
        count UInt32,
        hostname String,
        itemname String
    ) ENGINE = Distributed (
        '{cluster}',
        'glaber',
        'trends_uint_local',
        itemid
    );
        value_min Int64,
        value_max Int64,
        value_avg Int64,
        count UInt32,
        hostname String,
        itemname String
    ) ENGINE = Distributed (
        '{cluster}',
        'glaber',
        'trends_uint_local',
        itemid
    );
        itemid UInt64,
        clock DateTime,
        logeventid UInt64,
        source String,
        severity UInt8,
        hostname String,
        itemname String,
        ns UInt32,
        value String
    ) ENGINE = Distributed (
        '{cluster}',
        'glaber',
        'history_log_local',
        itemid
    );

--trends_dbl_local
CREATE TABLE
    glaber.trends_dbl_local ON CLUSTER glaber_cluster (
        day Date,
        itemid UInt64,
        clock DateTime,
        value_min Float64,
        value_max Float64,
        value_avg Float64,
        count UInt32,
        hostname String,
        itemname String
    ) ENGINE = ReplicatedMergeTree (
        '/glaber_cluster/tables/{shard}/glaber.trends_dbl_local',
        '{replica}'
    )
PARTITION BY
    toYYYYMM (day)
ORDER BY
    (itemid, clock) TTL day + toIntervalMonth (24);

--trends_dbl_global
CREATE TABLE
    glaber.trends_dbl ON CLUSTER glaber_cluster (
        day Date,
        itemid UInt64,
        clock DateTime,
        value_min Float64,
        value_max Float64,
        value_avg Float64,
        count UInt32,
        hostname String,
        itemname String
    ) ENGINE = Distributed ('{cluster}', 'glaber', 'trends_dbl_local', itemid);

--trends_uint_local
CREATE TABLE
    glaber.trends_uint_local ON CLUSTER glaber_cluster (
        day Date,
        itemid UInt64,
        clock DateTime,
        value_min Int64,
        value_max Int64,
        value_avg Int64,
        count UInt32,
        hostname String,
        itemname String
    ) ENGINE = ReplicatedMergeTree (
        '/glaber_cluster/tables/{shard}/glaber.trends_uint_local',
        '{replica}'
    )
PARTITION BY
    toYYYYMM (day)
ORDER BY
    (itemid, clock) TTL day + toIntervalMonth (24);

--trends_uint_global
CREATE TABLE
    glaber.trends_uint ON CLUSTER glaber_cluster (
        day Date,
        itemid UInt64,
        clock DateTime,
        value_min Int64,
        value_max Int64,
        value_avg Int64,
        count UInt32,
        hostname String,
        itemname String
    ) ENGINE = Distributed (
        '{cluster}',
        'glaber',
        'trends_uint_local',
        itemid
    );